<?php
use Symfony\Component\DomCrawler\Crawler;

require_once __DIR__ . '/vendor/autoload.php';

$client = new GuzzleHttp\Client();
$res = $client->request('GET', 'https://antenaplay.ro/');

if ($res->getStatusCode() == 200) {
    $crawler = new Crawler((string) $res->getBody());

    $node = $crawler->filter('head > title')->first()->text();
    var_dump($node);
}
